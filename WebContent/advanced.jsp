<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<html>
<head>
<title>ADVANCED SEARCH</title>
<meta name="viewport" content="width=device-width, initial-scale=1" />
<link rel="stylesheet" href="./css/main.css" />

<style>
.parr {
	background-image: url("./photos/XC.jpg");
	min-height: 800px;
	background-attachment: fixed;
	background-position: center;
	background-repeat: no-repeat;
	background-size: cover;
}


.value {
	border-bottom: 4px dashed #bdc3c7;
	text-align: center;
	font-weight: bold;
	font-size: 10em;
	width: 300px;
	height: 100px;
	line-height: 60px;
	margin: 40px auto;
	letter-spacing: -.07em;
	text-shadow: white 2px 2px 2px;
}

input[type="range"] {
	display: block;
	-webkit-appearance: none;
	background-color: #bdc3c7;
	width: 300px;
	height: 5px;
	border-radius: 5px;
	margin: 0 auto;
	outline: 0;
}

input[type="range"]::-webkit-slider-thumb {
	-webkit-appearance: none;
	background-color: #e74c3c;
	width: 30px;
	height: 30px;
	border-radius: 50%;
	border: 2px solid white;
	cursor: pointer;
	transition: .3s ease-in-out;
}

?
input[type="range"]::-webkit-slider-thumb:hover {
	background-color: white;
	border: 2px solid #e74c3c;
}

input[type="range"]::-webkit-slider-thumb:active {
	transform: scale(1.6);
}

.parallax {
	background-image: url("./photos/q7.jpg");
	min-height: 800px;
	background-attachment: fixed;
	background-position: center;
	background-repeat: no-repeat;
	background-size: cover;
}

.car {
	background-image: url("./photos/wrangler.jpg");
	min-height: 800px;
	background-attachment: fixed;
	background-position: center;
	background-repeat: no-repeat;
	background-size: cover;
}
</style>
</head>
<body>
	<div class="parr"></div>
	<div class="parallax">
		<form>
			<table border="1">
				<tr>
					<td style="color: white">FAMILY INCOME(monthly)
					<td>
					<td style="color: white"><input type="range" min="10000"
						max="100000" value="50" /></td>
					<td><p>
							<span class="value"></span>
						</p></td>
				</tr>

				<tr>
					<td style="color: white">PREFERENCES</td>
					<td style="color: white">SAFETY<input type="checkbox"
						name="a2" /></td>
					<td style="color: white">COMFORT<input type="checkbox"
						name="a2" /></td>
					<td style="color: white">RELIABILITY<input type="checkbox"
						name="a2" /></td>
					<td style="color: white">VALUE FOR MONEY<input type="checkbox"
						name="a2" /></td>
					<td style="color: white">OFF-ROADING<input type="checkbox"
						name="a2" /></td>
					<td style="color: white">UTILITY<input type="checkbox"
						name="a2" /></td>
				</tr>
				<tr>
					<td style="color: white">FUEL TYPE</td>
					<td style="color: white">PETROL<input type="checkbox"
						name="a3" /></td>
					<td style="color: white">DIESEL<input type="checkbox"
						name="a3" /></td>
					<td style="color: white">CNG<input type="checkbox" name="a3" /></td>
				</tr>

			</table>
		</form>
		<script>
			var elem = document.querySelector('input[type="range"]');

			var rangeValue = function() {
				var newValue = elem.value;
				var target = document.querySelector('.value');
				target.innerHTML = newValue;
			}

			elem.addEventListener("input", rangeValue);
		</script>
	</div>
	<div class="car"></div>
</body>
</html>
