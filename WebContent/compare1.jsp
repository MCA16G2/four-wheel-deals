<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ page import="java.sql.*" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
<style>
@import url('https://fonts.googleapis.com/css?family=Source+Sans+Pro');
body{
  background-image:url("./photos/car.jpg");
  font-family: 'Source Sans Pro', sans-serif;
}
#title{
  text-align: center;
  color:white;
  font-size:30px;
  text-transform: uppercase;
  margin-top: 100px;
}
.hover-table-layout {
  display: -webkit-box;
  display: -moz-box;
  display: -ms-flexbox;
  display: -webkit-flex;
  display: flex;
	 -webkit-flex-wrap: wrap;
    -moz-flex-wrap: wrap;
    -ms-flex-wrap: wrap;
    flex-wrap: wrap;
  max-width:800px;
  margin:0 auto;
}
.listing-item {
   display: block;
   width:100%;
	margin-bottom:20px;
    float: left;
    background: #fff;
    border-radius:10px;
    z-index:0;
	cursor:pointer;
    -webkit-transition: all 0.3s ease;
    -moz-transition: all 0.3s ease;
    transition: all 0.3s ease;
	    -webkit-box-shadow: 0px 1px 4px rgba(0, 0, 0, 0.10);
    -moz-box-shadow: 0px 1px 4px rgba(0, 0, 0, 0.10);
    box-shadow: 0px 1px 4px rgba(0, 0, 0, 0.10);

}
.listing-item:hover,  .listing-item.active{
	-webkit-transform: scale(1.03);
	-moz-transform: scale(1.03);
    transform: scale(1.03);
		-webkit-transition: all 0.3s;
	-moz-transition: all 0.3s;
    transition: all 0.3s;
	z-index:2;

}
.listing-item .listing{
    padding:20px;
    position:relative;	
}
.listing-item .listing:before{
    content:"";
	position:absolute;
	top:-15px;
	left:-o-calc(50% - 15px);
	left:-moz-calc(50% - 15px);
	left:-webkit-calc(50% - 15px);
	left:calc(50% - 15px);
	border-bottom:20px solid #fff;
	border-left:20px solid transparent;
	border-right:20px solid transparent;
}
	figure.image img {
		width:100%;
    border-top-left-radius: 10px;
    border-top-right-radius: 10px;
}
figure.image {
    position: relative;
	margin: 0;
    padding: 0;
}
figure.image figcaption {
    position: absolute;
    top: 0;
    width: 100%;
    text-align: center;
    bottom: 4px;
	background: rgba(0,0,0,0.6);
    border-top-left-radius: 10px;
    border-top-right-radius: 10px;

}
figcaption .caption{
	position:relative;
	top:50%;
	-moz-transform:translateY(-50%);
	-webkit-transform:translateY(-50%);
	transform:translateY(-50%);

}
figcaption h1{
  	color:white;
	font-weight:bold;
  	font-size:16px;
  text-transform: uppercase;
}
figcaption p{
  color:white;
  font-size:12px;
}
.listing h4 {
   
    font-size: 13px;
    text-align: center;
    padding: 5px 10px;
    font-weight: bold;
}
.listing h4:not(:last-child){
   border-bottom: 1px solid #ccc;
}
.listing-item:hover figure.image figcaption{
       background: rgba(195, 39, 43, 0.6);
}
@media only screen and (min-width:540px){
	.listing-item {
   display: block;
   width: -webkit-calc(100%/3);
   width: -moz-calc(100%/3);
    width: calc(100%/3);
	}
}
@media only screen and (min-width:1024px){
	.hover-table-layout{
        padding: 30px;
	}
}
</style>
</head>
<body>
<h2 align="center">COMPARE CAR</h2>
<% 
String carBrand1 = request.getParameter("sel_car_brand1");
String carModel1 = request.getParameter("sel_car_model1");

String carBrand2 = request.getParameter("sel_car_brand2");
String carModel2 = request.getParameter("sel_car_model2");
String url = "jdbc:mysql://localhost:3306/logintest";
String username = "root";
String password = "root";
String sql = "SELECT variant.id,variant.price,brand.brandname,model.modelname,variant.variant,variant.engine,variant.torque,variant.transmission,variant.seatingcapacity,variant.mileage,variant.Fueltype FROM variant INNER JOIN model ON variant.id=model.id INNER JOIN brand ON  model.brandid=brand.brandid AND model.id='"+carModel1+"'";
Class.forName("com.mysql.jdbc.Driver");
Connection con = DriverManager.getConnection(url, username, password);
Statement st = con.createStatement();
ResultSet rs = st.executeQuery(sql);
rs.next();

String sql1 = "SELECT variant.id,variant.price,brand.brandname,model.modelname,variant.variant,variant.engine,variant.torque,variant.transmission,variant.seatingcapacity,variant.mileage,variant.Fueltype FROM variant INNER JOIN model ON variant.id=model.id INNER JOIN brand ON  model.brandid=brand.brandid AND model.id='"+carModel2+"'";
Statement st1 = con.createStatement();
ResultSet rs1 = st1.executeQuery(sql1);
rs1.next();
%>
 
<div class="hover-table-layout">
    <div class="listing-item">
        <figure class="image">
            <img src="https://i.ytimg.com/vi/MTrzTABzLfY/maxresdefault.jpg" alt="image">
            <figcaption>
              <div class="caption">
                <h1><%=rs.getString("model.modelname") %></h1>
                
                </div>
            </figcaption>
        </figure>
        <div class="listing">
            
            <h4>Brand:<%=rs.getString("brand.brandname") %></h4>
            <h4>Model:<%=rs.getString("model.modelname") %></h4>
            <h4>Engine:<%=rs.getString("variant.engine") %></h4>
            <h4>Torque:<%=rs.getString("variant.torque") %></h4>
            <h4>Transmission:<%=rs.getString("variant.transmission") %></h4>
            <h4>Seating Capacity:<%=rs.getString("variant.seatingcapacity") %></h4>
            <h4>Mileage:<%=rs.getString("variant.mileage") %></h4>
            <h4>Model name:<%=rs.getString("model.modelname") %></h4>
            <h4>Fuel Type:<%=rs.getString("variant.Fueltype") %></h4>
            <h4>Price:<%=rs.getString("variant.price") %></h4>
            
        </div>
    </div>
    <div class="listing-item">
        <figure class="image">
            <img src="https://i.ytimg.com/vi/MTrzTABzLfY/maxresdefault.jpg" alt="image">
            <figcaption>
               <div class="caption">
                <h1><%=rs1.getString("model.modelname") %></h1>
               
                </div>
            </figcaption>
        </figure>
        <div class="listing">
            
            <h4>Brand:<%=rs1.getString("brand.brandname") %></h4>
            <h4>Model:<%=rs1.getString("model.modelname") %></h4>
            <h4>Engine:<%=rs1.getString("variant.engine") %></h4>
            <h4>Torque:<%=rs1.getString("variant.torque") %></h4>
            <h4>Transmission:<%=rs1.getString("variant.transmission") %></h4>
            <h4>Seating Capacity:<%=rs1.getString("variant.seatingcapacity") %></h4>
            <h4>Mileage:<%=rs1.getString("variant.mileage") %></h4>
            <h4>Model name:<%=rs1.getString("model.modelname") %></h4>
            <h4>Fuel Type:<%=rs1.getString("variant.Fueltype") %></h4>
            <h4>Price:<%=rs1.getString("variant.price") %></h4>
        </div>
    </div>
    
</div>
</body>
</html>

