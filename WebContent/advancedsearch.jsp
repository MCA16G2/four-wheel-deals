<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
<style>
body{
 background-image:url("./photos/advimg.jpg");
 font-family: 'Source Sans Pro', sans-serif;
  background-repeat: no-repeat;
   background-size:cover;
 }
 


}
.value {
  border-bottom: 4px dashed #bdc3c7;
  text-align: center;
  font-weight: bold;
  font-size: 10em;
  width: 300px; 
  height: 100px;
  line-height: 60px;
  margin: 40px auto;
  letter-spacing: -.07em;
  text-shadow: white 2px 2px 2px;
}
input[type="range"] {
  display: block;
  -webkit-appearance: none;
  background-color: #bdc3c7;
  width: 300px;
  height: 5px;
  border-radius: 5px;
  margin: 0 auto;
  outline: 0;
}
input[type="range"]::-webkit-slider-thumb {
  -webkit-appearance: none;
  background-color: #e74c3c;
  width: 30px;
  height: 30px;
  border-radius: 50%;
  border: 2px solid white;
  cursor: pointer;
  transition: .3s ease-in-out;
}​
  input[type="range"]::-webkit-slider-thumb:hover {
    background-color: white;
    border: 2px solid #e74c3c;
  }
  input[type="range"]::-webkit-slider-thumb:active {
    transform: scale(1.6);
  }
</style>
</head>
<body >
<h2 align="center" style="color:blue;,font-family:impact;">CUSTOMIZE YOUR SEARCH</h2>
<form name="advsearch" action="advaction.jsp" method="post">
<table border="1">
<tr><td>income(monthly)<td><td><input type="range" name="a1" step="0.1" min="1" max="5" value="2" /></td></tr>
<tr><td><p>value:<span class="value"></span></p></td></tr>
<tr><td>interested in</td><td>safety<input type="checkbox" name="a2" value="safety"/></td><td>comfort<input type="checkbox" name="a3" value="comfort"/></td><td>reliability<input type="checkbox" name="a4" value="relaibility"/></td><td>value for money<input type="checkbox" name="a5" value="value for money"/></td><td>offroading<input type="checkbox" name="a6" value="offroading"/></td><td>utility<input type="checkbox" name="a7" value="utility"/></td></tr>
<tr><td>fuel type</td><td>petrol<input type="checkbox" name="a8" value="petrol"/></td><td>diesel<input type="checkbox" name="a9" value="diesel"/></td><td>CNG<input type="checkbox" name="a10" value="CNG"/></td></tr>
<tr><td></td><td><input type ="submit">

</table>
</form>
<script>
var elem = document.querySelector('input[type="range"]');

var rangeValue = function(){
  var newValue = elem.value;
  var target = document.querySelector('.value');
  target.innerHTML = newValue;
}

elem.addEventListener("input", rangeValue);
</script>
</body>
</html>