function validate(){
	
	
	if(document.registration.firstname.value=="")
	{
	alert("please enter your firstname");
	document.registration.firstname.focus();
	return false;
	}
	
	if(document.registration.lastname.value=="")
	{
	alert("please enter your lastname");
	document.registration.lastname.focus();
	return false;
	}
	
	if(document.registration.email.value=="")
	{
	alert("please enter your email");
	document.registration.email.focus();
	return false;
	}
	
	if(document.registration.state.value=="")
	{
	alert("please enter your state");
	document.registration.state.focus();
	return false;
	}
	
	if(document.registration.city.value=="")
	{
	alert("please enter your city");
	document.registration.city.focus();
	return false;
	}
	
	if(document.registration.username.value=="")
	{
	alert("please enter your username");
	document.registration.username.focus();
	return false;
	}
	
	if(document.registration.password.value=="")
	{
	alert("please enter your password");
	document.registration.password.focus();
	return false;
	}
	
	
	
	var emailID = document.registration.email.value;
    atpos = emailID.indexOf("@");
    dotpos = emailID.lastIndexOf(".");
    
    if (atpos < 1 || ( dotpos - atpos < 2 )) 
    {
       alert("Please enter correct email ID")
       document.registration.email.focus() ;
       return false;
    }
    
	
   
	
	if(document.registration.password.value != document.registration.cnfpassword.value){
		alert("password didn't match,please reenter the password");
		document.registration.password.focus();
		return false;
	}
	return true;
}
