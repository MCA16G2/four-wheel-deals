<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<%@ page import="java.sql.*" %> 
<%@ page import="java.io.*" %> 
<%@page import="java.sql.DriverManager"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.Statement"%>
<%@page import="java.sql.Connection"%>
<%
String modelid = request.getParameter("id");
String driver = "com.mysql.jdbc.Driver";
String connectionUrl = "jdbc:mysql://localhost:3306/";
String database = "logintest";
String userid = "root";
String password = "samad123";
try {
Class.forName(driver);
} catch (ClassNotFoundException e) {
e.printStackTrace();
}
Connection connection = null;
Statement statement = null;
ResultSet resultSet = null;
%>
<html>
<body style="background-color: #ECE5B6;">
<h1 align="center">VARIANT TABLE</h1>
<form action = "insertvariant.jsp" method = "POST"><div align="center">
<input type = "submit" value = "ADD VARIANT" /></div>
 </form>
<div align="center"><table border="1">
<tr>
<td>ID</td>
<td>MODEL ID</td>
<td>VARIANT</td>
<td>ENGINE(CC)</td>
<td>POWER(BHP)</td>
<td>TORQUE(NM)</td>
<td>TRANSMISSION(AT/MT)</td>
<td>SEATING CAPACITY</td>
<td>MILEAGE</td>
<td>PRICE</td>
<td>FUEL TYPE</td>
<td>UPDATE</td>
<td>VIEW FEATURE</td>
<td>DELETE</td>
</tr>
<%
try{
connection = DriverManager.getConnection(connectionUrl+database, userid, password);
statement=connection.createStatement();
String sql = "select * from variant where variant.modelid="+modelid;
resultSet = statement.executeQuery(sql);
while(resultSet.next()){
%>
<tr>
<td><%=resultSet.getString(1) %></td>
<td><%=resultSet.getString("modelid") %></td>
<td><%=resultSet.getString("variant") %></td>
<td><%=resultSet.getString("engine") %></td>
<td><%=resultSet.getString("power") %></td>
<td><%=resultSet.getString("torque") %></td>
<td><%=resultSet.getString("transmission") %></td>
<td><%=resultSet.getString("seatingcapacity") %></td>
<td><%=resultSet.getString("mileage") %></td>
<td><%=resultSet.getString("price") %></td>
<td><%=resultSet.getString("fueltype") %></td>
<td><a href="updatevariant.jsp?id=<%=resultSet.getString(1)%>">update</a></td>
<td><a href="showfeat.jsp?id=<%=resultSet.getString(1)%>">view feature</a></td>
<td><a href="deletefeature.jsp?id=<%=resultSet.getString(1)%>">delete</a></td>
</tr>
<%
}
connection.close();
} catch (Exception e) {
e.printStackTrace();
}
%>
</table>
</div>
</body>
</html>