<%@page import="org.apache.mahout.cf.taste.recommender.RecommendedItem"%>
<%@page import="com.prediction.org.Recommenderapp.Recommender"%>
<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ page import="java.sql.*" %>
    
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
<title>Insert title here</title>
<style>

#val {
	font-family: century gothic;
}

#font {
	font-family: century gothic;
	overflow: hidden;
	background-color: black;
	z-index: 4;
}

#sign {
	border-right: 1px solid white;
}

.car {
	width: 70%;
	height: 30%;
}

.btn-default {
	background-color: black;
	color: white;
	height: 80px;
	width: 100px;
	border: 0px solid black;
}

#btn:hover {
	opacity: 0.1;
	color: white;
	background-color: black;
	color: white;
}



.tab-content {
	padding: 16px;
}

.sticky {
	position: fixed;
	top: 0;
	width: 100%;
}

.sticky+.content {
	padding-top: 60px;
}

.btn1 {
	background-color: black;
	color: white;
	height: 80px;
	width: 120px;
	border: 0px solid black;
}




@import url('https://fonts.googleapis.com/css?family=Source+Sans+Pro');
body{
  background-image:url("./photos/car3.jpg");
  font-family: 'Source Sans Pro', sans-serif;
  background-repeat: no-repeat;
   background-size:cover;
}
#title{
  text-align: center;
  color:white;
  font-size:30px;
  text-transform: uppercase;
  margin-top: 100px;
}
.hover-table-layout {
  display: -webkit-box;
  display: -moz-box;
  display: -ms-flexbox;
  display: -webkit-flex;
  display: flex;
	 -webkit-flex-wrap: wrap;
    -moz-flex-wrap: wrap;
    -ms-flex-wrap: wrap;
    flex-wrap: wrap;
  max-width:800px;
  margin:0 auto;
}
.listing-item {
   display: block;
   width:100%;
	margin-bottom:20px;
    float: left;
    background: #fff;
    border-radius:10px;
    z-index:0;
	cursor:pointer;
    -webkit-transition: all 0.3s ease;
    -moz-transition: all 0.3s ease;
    transition: all 0.3s ease;
	    -webkit-box-shadow: 0px 1px 4px rgba(0, 0, 0, 0.10);
    -moz-box-shadow: 0px 1px 4px rgba(0, 0, 0, 0.10);
    box-shadow: 0px 1px 4px rgba(0, 0, 0, 0.10);

}
.listing-item:hover,  .listing-item.active{
	-webkit-transform: scale(1.03);
	-moz-transform: scale(1.03);
    transform: scale(1.03);
		-webkit-transition: all 0.3s;
	-moz-transition: all 0.3s;
    transition: all 0.3s;
	z-index:2;

}
.listing-item .listing{
    padding:20px;
    position:relative;	
}
.listing-item .listing:before{
    content:"";
	position:absolute;
	top:-15px;
	left:-o-calc(50% - 15px);
	left:-moz-calc(50% - 15px);
	left:-webkit-calc(50% - 15px);
	left:calc(50% - 15px);
	border-bottom:20px solid #fff;
	border-left:20px solid transparent;
	border-right:20px solid transparent;
}
	figure.image img {
		width:100%;
    border-top-left-radius: 10px;
    border-top-right-radius: 10px;
}
figure.image {
    position: relative;
	margin: 0;
    padding: 0;
}
figure.image figcaption {
    position: absolute;
    top: 0;
    width: 100%;
    text-align: center;
    bottom: 4px;
	background: rgba(0,0,0,0.6);
    border-top-left-radius: 10px;
    border-top-right-radius: 10px;

}
figcaption .caption{
	position:relative;
	top:50%;
	-moz-transform:translateY(-50%);
	-webkit-transform:translateY(-50%);
	transform:translateY(-50%);

}
figcaption h1{
  	color:white;
	font-weight:bold;
  	font-size:16px;
  text-transform: uppercase;
}
figcaption p{
  color:white;
  font-size:12px;
}
.listing h4 {
   
    font-size: 13px;
    text-align: center;
    padding: 5px 10px;
    font-weight: bold;
}
.listing h4:not(:last-child){
   border-bottom: 1px solid #ccc;
}
.listing-item:hover figure.image figcaption{
       background: rgba(195, 39, 43, 0.6);
}
@media only screen and (min-width:540px){
	.listing-item {
   display: block;
   width: -webkit-calc(100%/3);
   width: -moz-calc(100%/3);
    width: calc(100%/3);
	}
}
@media only screen and (min-width:1024px){
	.hover-table-layout{
        padding: 30px;
	}
}

</style>


</head>
<body>
<div class="container">

<form name="logout" action="logout.jsp"><div align="right"><button  type="submit"  class="btn btn-warning" >LOGOUT</button></div>
<h2 style="font-size:72px;color:yellow;">
<%
String name=request.getParameter("name");
out.println("Hai\t"+name);

%>
</h2>

<!-- Nav tabs -->
		<ul class="nav nav-tabs "  role="tablist">
			<li class="nav-item"><a style="color: grey"
				class="nav-link active" data-toggle="tab" href="#home">HOME</a></li>
			<li class="nav-item"><a style="color: grey" class="nav-link"
				data-toggle="tab" href="#menu1">CAR GALLERY</a></li>
			<li class="nav-item"><a style="color: grey" class="nav-link"
				data-toggle="tab" href="#menu2">COMPARE CAR</a></li>
		</ul>
		<!-- Tab panes -->
		<div class="tab-content">
			<div id="home" class="container tab-pane active">
				<br>
				<form name="advsearch">
				<button onclick="window.location.href='advancedsearch.jsp'" style="font-color: white;" type="button" id="btn">Advanced
					Search</button>
					</form>

			</div>
			<div id="menu1" class="container tab-pane fade">
				<br>
				<h3>CAR GALLERY</h3>
				<%
					String url = "jdbc:mysql://localhost:3306/logintest";
					String username = "root";
					String password = "root";
					String sql0 = "SELECT variant.id,variant.price,brand.brandname,model.modelname,variant.variant,variant.engine,variant.torque,variant.transmission,variant.seatingcapacity,variant.mileage,variant.Fueltype FROM variant INNER JOIN model ON variant.id=model.id INNER JOIN brand ON  model.brandid=brand.brandid";
							
					Class.forName("com.mysql.jdbc.Driver");
					Connection con0 = DriverManager.getConnection(url, username, password);
					Statement st0 = con0.createStatement();
					ResultSet rs0 = st0.executeQuery(sql0);

					while (rs0.next()) {
				%>
				<div class="card" style="width: 1000px; background-color: #85adad;">
					<img class="card-img-top" src=""
						alt="Card image" style="width: 50%; height: 50%;"
						data-toggle="tooltip" title="VIEW DETAILS">
					<div class="card-body">
						<h4 class="card-title"><%=rs0.getString("model.modelname")%></h4>


						<button type="button" class="btn btn-primary"
							data-toggle="collapse"
							data-target="#description-<%=rs0.getString("variant.id")%>">show or
							hide description</button>
						<div id="description-<%=rs0.getString("variant.id")%>" class="collapse">
							<p class="card-text">
								Model Name:<%=rs0.getString("model.modelname")%><br>
								brand name:<%=rs0.getString("brand.brandname") %><br>
								Variant Name:<%=rs0.getString("variant.variant") %><br>
								Engine:<%=rs0.getString("variant.engine") %><br>
								Torque:<%=rs0.getString("variant.torque") %><br>
								Transmission:<%=rs0.getString("variant.transmission") %><br>
								Seating Capacity:<%=rs0.getString("variant.seatingcapacity") %><br>
								mileage:<%=rs0.getString("variant.mileage") %><br>
								Fuel Type:<%=rs0.getString("variant.Fueltype") %><br>
								Price:<%=rs0.getString("variant.price")%>
								 
							</p>
						</div>
						<br> <br>
						
						
						
						<button value="<%=rs0.getString(1)%>" type="button"
							class="btn btn-warning " data-toggle="modal"
							data-target="#demo-<%=rs0.getString(1)%>">view fullsize</button>
					</div>




					<div class="modal fade in" id="demo-<%=rs0.getString(1)%>"
						role="dialog">
						<div class="modal-dialog">
							<div class="modal-body">
								<button class="close" data-dismiss="modal">X</button>
								<img style="margin-align: center" src="<%=rs0.getString(4)%>"
									width="180%" height="180%">


							</div>
						</div>
					</div>
				</div>




				<%
					}
				%>

</form>
			</div>
			<div id="menu2" class="container tab-pane fade">
				<br>
				<h3 style="color:red;">COMPARE CAR</h3>

				<form name="carselect" action="compare1.jsp" method="POST">
					<div class="form-group">


						<label for="sel1"><font style="color:red;">Select first choice</font></label> <select
							name="sel_car_brand1" class="form-control" id="sel1">
							<option value="">select brand</option>

							<option value=""></option>


						</select>
					</div>
					<div>
						<select name="sel_car_model1" class="form-control" id="sel2">
							<option selected="selected">Select model</option>
						</select>
					</div>

					<div class="form-group">

						<label for="sel3"><font style="color:red;">Select second choice</font></label> <select
							name="sel_car_brand2" class="form-control"
							onchange="brand_change2()" id="sel3">
							<option selected="selected">select brand</option>

							<option value=""></option>


						</select> <br> <select name="sel_car_model2" class="form-control"
							id="sel4">
							<option selected="selected">Select model</option>
						</select> <br>

						<div align="center">
							<button id="compare1" style="font-size: 15px;" type="submit"
								class="btn1">COMPARE</button>
							<input type="hidden" name="selectbox1" id="selectbox1">
							<div id="display cars"></div>





						</div>
					</div>

				</form>




				<p></p>
			</div>
		</div>
<hr>
<div  align="center" id="recommendations">
<font  style="color:aqua;font-size:16px">RECOMMENDATIONS FOR YOU</font>
<div class="hover-table-layout">

<% 


Class.forName("com.mysql.jdbc.Driver");
Connection con = DriverManager.getConnection(url, username, password);
String sql1 = "select * from login where username='"+name+"'";

Statement st1 = con.createStatement();
ResultSet rs1 = st1.executeQuery(sql1);
rs1.next();
long userid=Long.parseLong(rs1.getString(1));

Recommender recommender  = new Recommender();
List<RecommendedItem> recommendations = recommender.getRecommendations(userid, 3);
for(RecommendedItem item : recommendations ) {
%>



<% 

  
String url1 = "jdbc:mysql://localhost:3306/logintest";
String username1 = "root";
String password1= "root";
Class.forName("com.mysql.jdbc.Driver");
Connection con1 = DriverManager.getConnection(url1, username1, password1);


	String sql4 = "SELECT variant.id,variant.price,brand.brandname,model.modelname,variant.variant,variant.engine,variant.torque,variant.transmission,variant.seatingcapacity,variant.mileage,variant.Fueltype FROM variant INNER JOIN model ON variant.id=model.id INNER JOIN brand ON  model.brandid=brand.brandid AND model.id='"+item.getItemID()+"'";
	
	Statement st4= con1.createStatement();
	ResultSet rs4= st4.executeQuery(sql4);
	rs4.next();

	
%>		

 <div class="listing-item">
        <figure class="image">
            <img src="https://i.ytimg.com/vi/MTrzTABzLfY/maxresdefault.jpg" alt="image">
            <figcaption>
              <div class="caption">
                
                <p><%=rs4.getString("model.modelname") %></p>
                </div>
            </figcaption>
        </figure>
        <div class="listing">
             <h4>Brand:<%=rs4.getString("brand.brandname") %></h4>
            <h4>Model:<%=rs4.getString("model.modelname") %></h4>
            <h4>Engine:<%=rs4.getString("variant.engine") %></h4>
            <h4>Torque:<%=rs4.getString("variant.torque") %></h4>
            <h4>Transmission:<%=rs4.getString("variant.transmission") %></h4>
            <h4>Seating Capacity:<%=rs4.getString("variant.seatingcapacity") %></h4>
            <h4>Mileage:<%=rs4.getString("variant.mileage") %></h4>
            <h4>Model name:<%=rs4.getString("model.modelname") %></h4>
            <h4>Fuel Type:<%=rs4.getString("variant.Fueltype") %></h4>
            <h4>Price:<%=rs4.getString("variant.price") %></h4>
        </div>
    </div>

<%


  
  }

%>

</div>
</div>
<script>
		window.onscroll = function() {
			myFunction()
		};

		var navbar = document.getElementById("font");
		var sticky = navbar.offsetTop;

		function myFunction() {
			if (window.pageYOffset >= sticky) {
				navbar.classList.add("sticky")
			} else {
				navbar.classList.remove("sticky");
			}
		}
	</script>
	<script>
		$(document)
				.ready(
						function() {

							load_json_data('sel1');

							function load_json_data(id, parent_id) {
								var html_code = '';
								$
										.getJSON(
												'selectboxdata.json',
												function(data) {

													html_code += '<option value="">Select '
															+ id + '</option>';
													$
															.each(
																	data,
																	function(
																			key,
																			value) {
																		if (id == 'sel1') {
																			if (value.parent_id == '0') {
																				html_code += '<option value="'+value.id+'">'
																						+ value.name
																						+ '</option>';
																			}
																		} else {
																			if (value.parent_id == parent_id) {
																				html_code += '<option value="'+value.id+'">'
																						+ value.name
																						+ '</option>';
																			}
																		}
																	});
													$('#' + id).html(html_code);
												});

							}

							$(document)
									.on(
											'change',
											'#sel1',
											function() {
												var brand_id = $(this).val();
												if (brand_id != '') {
													load_json_data('sel2',
															brand_id);
												} else {
													$('#sel2')
															.html(
																	'<option value="">Select model</option>');

												}

											});

						});
	</script>
	<script>
		$(document)
				.ready(
						function() {

							load_json_data('sel3');

							function load_json_data(id, parent_id) {
								var html_code = '';
								$
										.getJSON(
												'selectboxdata.json',
												function(data) {

													html_code += '<option value="">Select '
															+ id + '</option>';
													$
															.each(
																	data,
																	function(
																			key,
																			value) {
																		if (id == 'sel3') {
																			if (value.parent_id == '0') {
																				html_code += '<option value="'+value.id+'">'
																						+ value.name
																						+ '</option>';
																			}
																		} else {
																			if (value.parent_id == parent_id) {
																				html_code += '<option value="'+value.id+'">'
																						+ value.name
																						+ '</option>';
																			}
																		}
																	});
													$('#' + id).html(html_code);
												});

							}

							$(document)
									.on(
											'change',
											'#sel3',
											function() {
												var brand_id = $(this).val();
												if (brand_id != '') {
													load_json_data('sel4',
															brand_id);
												} else {
													$('#sel4')
															.html(
																	'<option value="">Select model</option>');

												}
											});
						});
	</script>
	<script>
var elem = document.querySelector('input[type="range"]');

var rangeValue = function(){
  var newValue = elem.value;
  var target = document.querySelector('.value');
  target.innerHTML = newValue;
}

elem.addEventListener("input", rangeValue);
</script>
	</div>	
</body>
</html>