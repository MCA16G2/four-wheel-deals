<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<title>Welcome to FOUR WHEEL DEALS</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>

<style>
/* Make the image fully responsive */
.carousel-inner img {
	width: 85%;
	height: 10%;
}
#img
{
background-repeat: no-repeat;
}

#val {
	font-family: century gothic;
}

#font {
	font-family: century gothic;
}

#sign {
	border-right: 1px solid white;
}

.car {
	width: 70%;
	height: 30%;
}

.btn-default {
	background-color: black;
	color: white;
	height: 80px;
	width: 120px;
	border: 0px solid black;
}

.btn-primary {
	background-color: black;
	color: white;
	height: 80px;
	width: 120px;
	border: 0px solid black;
}

#btn:hover {
	opacity: 0.1;
	color: white;
	background-color: black;
	color: white;
}
</style>
</head>
<body id="img" background="./photos/dash.jpg" />
<div class="container">



	<div style="margin-left: 80%" id="val" class="btn-group">

		<button onClick="window.location.href='signup.jsp'" type="button"
			id="sign" class="btn btn-default">S I G N U P</button>
		<form name="login">
			<button onClick="window.location.href='Login.jsp'" type="button"
				class="btb btn-default">L O G I N</button>
		</form>

	</div>
	<br> <br> <br> <br> <br> <br>
	<div id="demo" class="carousel slide" data-ride="carousel">

		<!-- Indicators -->
		<ul class="carousel-indicators">
			<li data-target="#demo" data-slide-to="0" class="active"></li>
			<li data-target="#demo" data-slide-to="1"></li>
			<li data-target="#demo" data-slide-to="2"></li>
		</ul>

		<!-- The slideshow -->
		<div class="carousel-inner">
			<div class="carousel-item active">
				<img src="./photos/bmw.jpg" alt="Los Angeles" class="car"
					width="1100px" height="40">
			</div>
			<div class="carousel-item">
				<img src="./photos/audia6.jpg" alt="Chicago" class="car"
					width="1100px" height="40">
			</div>
			<div class="carousel-item">
				<img src="./photos/mercedesbenz.jpg" alt="New York" class="car"
					width="1100px" height="40">
			</div>
		</div>

		<!-- Left and right controls -->
		<a class="carousel-control-prev" href="#demo" data-slide="prev"> <span
			class="carousel-control-prev-icon"></span>
		</a> <a class="carousel-control-next" href="#demo" data-slide="next">
			<span class="carousel-control-next-icon"></span>
		</a>
	</div>

	<!-- Nav tabs -->
	<ul class="nav nav-tabs " id="font" role="tablist">
		<li class="nav-item"><a style="color: grey"
			class="nav-link active" data-toggle="tab" href="#home">HOME</a></li>
		<li class="nav-item"><a style="color: grey" class="nav-link"
			data-toggle="tab" href="#menu1">CAR GALLERY</a></li>
		<li class="nav-item"><a style="color: grey" class="nav-link"
			data-toggle="tab" href="#menu2">COMPARE CAR</a></li>
	</ul>

	<!-- Tab panes -->
	<div class="tab-content">
		<div id="home" class="container tab-pane active">
		<div  style="background:transparent;background-color:#4dc3ff;" class="jumbotron">
		<h1 align="center" style="font-family:century gothic;color:white;">Welcome to Four Wheel Deals</h1>
		<p>
		</div>
			
			

		</div>
		<div id="menu1" class="container tab-pane fade">
			<br>
			<h3>CAR GALLERY</h3>

		</div>
		<div id="menu2" class="container tab-pane fade">
			<br>
			<h3>COMPARE CAR</h3>
			<br>
			<div class="form-group">
				<form name="carselect">
					<label for="sel1">Select your first choice</label> <select name="car1"
						class="form-control" id="sel1">
						<option>Select a brand</option>
						<option>AUDI</option>
						<option>BMW</option>
						<option>MERCEDES BENZ</option>
						<option>SUZUKI</option>
						<option>FORD</option>
						</select>
				</form>


			</div>
			<div class="form-group">
				<label for="sel1"></label> <select class="form-control"
					id="sel1">
					<option>Select a model</option>
					<option>A3</option>
					<option></option>
					<option>S CLASS</option>
					<option>SWIFT</option>
					<option>FIGO</option>

				</select>
				<br>
				<br>
					<div class="form-group">
				<label for="sel1">Select your second choice</label> <select class="form-control"
					id="selllll">
					<option>Select a brand</option>
					<option>A3</option>
					<option></option>
					<option>S CLASS</option>
					<option>SWIFT</option>
					<option>FIGO</option>
					</select>
					<br>
					
						<div class="form-group">
				<label for="sel1"></label> <select class="form-control"
					id="selll">
					<option>Select a model</option>
					<option>A3</option>
					<option></option>
					<option>S CLASS</option>
					<option>SWIFT</option>
					<option>FIGO</option>
					</select>
				
			</div>
			<div align="center">
				<button style="font-size: 15px;" type="button"
					class="btn btn-primary">COMPARE</button>
			</div>
		</div>
	</div>
</html>