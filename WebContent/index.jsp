<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ page import="java.sql.*"%>
<!DOCTYPE html>
<html>
<head>
<title>Insert title here</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
<script
	src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.9/angular.min.js"></script>

<style>
/* Make the image fully responsive */
.carousel-inner img {
	width: 85%;
	height: 10%;
}

#val {
	font-family: century gothic;
}

#font {
	font-family: century gothic;
	overflow: hidden;
	background-color: black;
	z-index: 100;
}

#sign {
	border-right: 1px solid white;
}

.car {
	width: 70%;
	height: 30%;
}

.btn-default {
	background-color: black;
	color: white;
	height: 80px;
	width: 100px;
	border: 0px solid black;
}

#btn:hover {
	opacity: 0.1;
	color: white;
	background-color: black;
	color: white;
}

body {
	background-repeat: no-repeat;
}

.tab-content {
	padding: 16px;
}

.sticky {
	position: fixed;
	top: 0;
	width: 100%;
}

.sticky+.content {
	padding-top: 60px;
}

.btn1 {
	background-color: black;
	color: white;
	height: 80px;
	width: 120px;
	border: 0px solid black;
}
</style>

</head>
<body background="./photos/dash.jpg">
	<div class="container">




		<div style="margin-left: 80%" id="val" class="btn-group">

			<form name="signup">
				<button onClick="window.location.href='signin.jsp'" type="button"
					id="sign" class="btn btn-default">S I G N U P</button>
			</form>
			<form name="login">
				<button onClick="window.location.href='Login.jsp'" type="button"
					class="btb btn-default">L O G I N</button>
			</form>

		</div>
		<br> <br> <br> <br> <br> <br>
		<div id="demo" class="carousel slide" data-ride="carousel">

			<!-- Indicators -->
			<ul class="carousel-indicators">
				<li data-target="#demo" data-slide-to="0" class="active"></li>
				<li data-target="#demo" data-slide-to="1"></li>
				<li data-target="#demo" data-slide-to="2"></li>
			</ul>

			<!-- The slideshow -->
			<div class="carousel-inner">
				<div class="carousel-item active">
					<img src="./photos/bmw.jpg" alt="Los Angeles" class="car"
						width="1100px" height="40">
				</div>
				<div class="carousel-item">
					<img src="./photos/audia6.jpg" alt="Chicago" class="car"
						width="1100px" height="40">
				</div>
				<div class="carousel-item">
					<img src="./photos/mercedesbenz.jpg" alt="New York" class="car"
						width="1100px" height="40">
				</div>
			</div>

			<!-- Left and right controls -->
			<a class="carousel-control-prev" href="#demo" data-slide="prev">
				<span class="carousel-control-prev-icon"></span>
			</a> <a class="carousel-control-next" href="#demo" data-slide="next">
				<span class="carousel-control-next-icon"></span>
			</a>
		</div>

		

		
	</div>
	
	

	
	
</body>
</html>

