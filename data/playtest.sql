/*
SQLyog Community v12.4.3 (64 bit)
MySQL - 8.0.12 : Database - playtest
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`playtest` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci */;

USE `playtest`;

/*Table structure for table `car` */

DROP TABLE IF EXISTS `car`;

CREATE TABLE `car` (
  `id` int(11) NOT NULL,
  `name` varchar(30) DEFAULT NULL,
  `price` varchar(30) DEFAULT NULL,
  `image` blob,
  `brand` varchar(45) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `brandid` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `brandref` (`brandid`),
  CONSTRAINT `brandref` FOREIGN KEY (`brandid`) REFERENCES `model` (`brandid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

/*Data for the table `car` */

insert  into `car`(`id`,`name`,`price`,`image`,`brand`,`brandid`) values 
(1,'mercedez','500000','http://localhost:8080/FOUR_WHEEL_DEALS/cardbimages/lamborgini.jpg','mercedez benz',1),
(2,'lamborgini','200000','http://localhost:8080/FOUR_WHEEL_DEALS/cardbimages/marrusia.jpg','audi',2),
(3,'marussia','300000','http://localhost:8080/FOUR_WHEEL_DEALS/cardbimages/mclaren.jpg','bmw',3);

/*Table structure for table `login` */

DROP TABLE IF EXISTS `login`;

CREATE TABLE `login` (
  `id` int(11) NOT NULL,
  `firstname` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `lastname` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `Email` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `State` varchar(50) DEFAULT NULL,
  `city` varchar(50) DEFAULT NULL,
  `username` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `password` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

/*Data for the table `login` */

insert  into `login`(`id`,`firstname`,`lastname`,`Email`,`State`,`city`,`username`,`password`) values 
(1,'sreenath','gr','gr.sreenath@gmail.com','kerala','koodallur','sreenath','1234'),
(2,'bharath','d','iambharath07@gmail.com','kerala','trivandrum','bharath','123'),
(3,'Shahala','kareem','shahalakareem95@gmail.com','kerala','malappuram','shahala','1234'),
(4,'abdul','samad','abdulsamadev@gmail.com','kerala','malappuram','samad','12345');

/*Table structure for table `model` */

DROP TABLE IF EXISTS `model`;

CREATE TABLE `model` (
  `brandid` int(11) NOT NULL,
  `brandname` varchar(35) DEFAULT NULL,
  PRIMARY KEY (`brandid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

/*Data for the table `model` */

insert  into `model`(`brandid`,`brandname`) values 
(1,'mercedez benz'),
(2,'audi'),
(3,'bmw'),
(4,'ford'),
(5,'tesla');

/*Table structure for table `userpreference` */

DROP TABLE IF EXISTS `userpreference`;

CREATE TABLE `userpreference` (
  `userid` int(11) DEFAULT NULL,
  `caritemid` int(11) DEFAULT NULL,
  `preference` float DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

/*Data for the table `userpreference` */

insert  into `userpreference`(`userid`,`caritemid`,`preference`) values 
(1,2,3.5),
(2,3,4.5),
(2,5,3.5),
(3,4,4.5),
(3,8,2.5),
(3,2,3.6),
(5,6,2.3),
(1,5,2.6),
(1,4,3.2),
(3,5,3.4),
(4,2,2.5),
(4,4,3.2),
(4,10,2.4),
(1,12,3.4),
(2,14,2.2),
(2,13,3.2),
(2,9,4.2),
(3,9,3.3),
(1,23,3.5),
(2,24,4.5),
(2,25,4.6),
(3,33,3.6),
(4,42,4.5),
(2,4,5.6),
(2,3,2.5),
(3,3,3.3),
(4,4,4.3),
(1,7,4.2),
(4,5,3.2),
(3,5,4.2),
(2,7,4),
(3,16,3),
(15,54,2.5),
(16,34,4.2);

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
