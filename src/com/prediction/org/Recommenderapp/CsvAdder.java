 package com.prediction.org.Recommenderapp;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;




public class CsvAdder {
	public static void main(String args[]) throws IOException, SQLException {
		try {
		    String filename ="data/dataset2.csv";
            FileWriter fw = new FileWriter(filename);
            Class.forName("com.mysql.jdbc.Driver");
            Connection conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/playtest", "root", "root");
            String query = "select * from userpreference";
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(query);
            while (rs.next()) {
                fw.append(rs.getString(1));
                fw.append(',');
                fw.append(rs.getString(2));
                fw.append(',');
                fw.append(rs.getString(3));
                fw.append('\n');
                
            }
            fw.flush();
            fw.close();
            conn.close();
           
        }
		catch(ClassNotFoundException e) {
			e.printStackTrace();
		}
		catch(NullPointerException e) {
			e.printStackTrace();
		}
		
	}
	

}
